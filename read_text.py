def read_file_line(filename):
    """
    Reads a line from a file
    :param filename: file to read
    :return: next line from file
    """
    with open(filename, 'r') as f:
        line = f.readline()

    return line

def read_file_lines(filename):
    """
    Reads a file's lines
    :param filename: file to read
    :return: list of lines from file
    """
    with open(filename, 'r') as f:
        lines = f.readlines()

    return lines

def contains_blue_devil(filename):
    """
    Reads a file to see if it contains the phrase "blue devil"
    :return: true if contains the phrase; false o.w.
    """
    BLUE_DEVIL = "Blue Devil"
    with open(filename, 'r') as f:
        for line in f.readlines():
            if (BLUE_DEVIL in line):
                return True
    return False

def find_non_terminal_said(filename):
    import re

    word = "said"
    expr = "said\\b[^.?!]"
    regexExpr = re.compile(expr)

    str = "hello"
    str = str[:1] + str[1:3].upper() + str[3:];
    print(str)

    with open(filename, 'r') as f:
        text = f.read()
        saids = re.findall(regexExpr, text)
        list = []
        for s in saids:
            list.append(s[:len(word)])
        return list


if __name__ == "__main__":
    print(read_file_line("example_text.txt"))
    print(read_file_lines("example_text.txt"))
    print(contains_blue_devil("example_text.txt"))
    print(contains_blue_devil("blue_devil.txt"))
    print(find_non_terminal_said("example_text.txt"))